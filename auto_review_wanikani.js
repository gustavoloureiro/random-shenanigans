const puppeteer = require('puppeteer');
const hiraganas_dict = require('./hiraganas_dict');
var fs = require('fs');

(async () => {

    // gets the dict of hiraganas
    HIRAGANAS = hiraganas_dict.HIRAGANAS

    //Puppeteer Initialization
    const browser = await puppeteer.launch({headless: false})
    const page1 = await browser.newPage()
    await page1.setViewport({
        width: 1920,
        height: 1080,
        deviceScaleFactor: 1,
    })
    const page2 = await browser.newPage()
    await page2.setViewport({
        width: 1920,
        height: 1080,
        deviceScaleFactor: 1,
    })

    // Access Jisho and then Wanikani reviews
    await page1.goto('https://jisho.org/')
    await page2.bringToFront()
    await page2.goto('https://www.wanikani.com/login')
    await page2.type('#user_login', process.env.USER, {delay: 30})
    await page2.type('#user_password', process.env.PASSWORD, {delay: 30})
    await Promise.all([page2.click('button[type=submit]'), page2.waitForNavigation()]);
    await Promise.all([page2.goto('https://www.wanikani.com/review/start'), page2.waitForNavigation()]);

    // Begins review loop
    let searchTerm = ''
    let currAnswer = ''
    let question_type = ''
    let text = fs.readFileSync('dict_wanikani.txt').toString();
    let myDict = JSON.parse(text);
    while(await page2.$('#user-response') !== null) {

        // Discovers question type
        await page2.$('#question-type') !== null
        await page2.waitForFunction(() => {
            return document.getElementById('question-type').innerText.includes("Vocabulary") ||
            document.getElementById('question-type').innerText.includes("Kanji") ||
            document.getElementById('question-type').innerText.includes("Radical")
        }, { timeout: 0 })
        question_type = await page2.$eval('#question-type', e => e.innerText);
        searchTerm = await page2.$eval('#character', e => e.innerText)

        // Checks if answer is already on Dict
        if(question_type + ' - ' + searchTerm in myDict) {
            currAnswer = myDict[question_type + ' - ' + searchTerm]
            await page2.type('#user-response', currAnswer, {delay: 30})
        } else {
            // Searchs Jisho
            await page1.bringToFront()
            await page1.type('#keyword', searchTerm, {delay: 30})
            await Promise.all([page1.click('button[type=submit]'), page1.waitForNavigation()]);

            // Five types of questions
            if (question_type === "Vocabulary Reading") {
                currAnswer = await page1.evaluate((searchTerm, HIRAGANAS) => {
                    let my_return = ''
                    let has_hiragana = false
                    for(let j = 0; j < searchTerm.length; j ++) {
                        if(HIRAGANAS.includes(searchTerm.charAt(j))) {
                            has_hiragana = true
                        }
                    }
                    if (has_hiragana) {
                        let array_furigana = document.getElementsByClassName('concept_light-representation')[0].getElementsByClassName('furigana')
                        for (let k = 0; k < array_furigana[0].getElementsByTagName('span').length; k++) {
                            if (array_furigana[0].getElementsByTagName('span')[k].innerText === '') {
                                my_return += searchTerm[k]
                            } else {
                                my_return += array_furigana[0].getElementsByTagName('span')[k].innerText.split('(')[0]
                            }
                        }
                    } else {
                        my_return = document.getElementsByClassName("furigana")[0].innerText.split('(')[0]
                    }
                    return my_return
                }, searchTerm, HIRAGANAS)
                await page2.bringToFront()
                await page2.type('#user-response', currAnswer, {delay: 30})
            } else if(question_type === "Vocabulary Meaning" || question_type === "Kanji Meaning") {
                currAnswer = await page1.evaluate(() => {
                    if (document.getElementsByClassName('meaning-meaning')[0].innerText.indexOf(";") != -1) {
                        return document.getElementsByClassName('meaning-meaning')[0].innerText.split(";")[0]
                    } else {
                        return document.getElementsByClassName('meaning-meaning')[0].innerText.split('(')[0]
                    }
                })
                await page2.bringToFront()
                await page2.type('#user-response', currAnswer, {delay: 30})
            } else if(question_type === "Kanji Reading") {
                let temp_url = await page1.evaluate(() => {
                    return document.getElementsByClassName('on readings')[0].getElementsByTagName('span')[1].getElementsByTagName('a')[0].href;
                })
                await Promise.all([page1.goto(temp_url), page1.waitForNavigation()]);
                currAnswer = await page1.evaluate(() => {return document.getElementsByClassName("furigana")[0].innerText.split('(')[0]})
                await page2.bringToFront()
                await page2.type('#user-response', currAnswer, {delay: 30})
            } else if(question_type === "Radical Name") {
                await page2.bringToFront()
                await page2.type('#user-response', "peixe", {delay: 30})
            }
        }

        // Waits for me to submit an answer
        await page2.waitForFunction(() => { return document.getElementById("user-response").disabled }, { timeout: 0 })
        await page2.waitFor(500)
        wrongAnswer = await page2.evaluate(() => {
            return window.getComputedStyle(document.getElementById("user-response")).backgroundColor === "rgb(255, 0, 51)"
        })
        if(!wrongAnswer) {
            // Salva no dicionário
            myDict[question_type + ' - ' + searchTerm] = await page2.evaluate(() => {return document.getElementById("user-response").value})
            fs.writeFile('dict_wanikani.txt', JSON.stringify(myDict), (err) => {
                if (err) throw err;
            });
        }
        await page2.keyboard.press('Enter')
        await page2.waitForFunction(() => { return !document.getElementById("user-response").disabled }, { timeout: 0 })
    }

    // Close
    await browser.close()
})()